from django.shortcuts import render
from rest_framework.generics import CreateAPIView, ListAPIView
from django.views.generic import TemplateView
from .serializers import RecordSerialiser
from .models import Record
# Create your views here.


class CreateRecord(CreateAPIView):
    serializer_class = RecordSerialiser

class RetrieveRecordList(ListAPIView):
    serializer_class = RecordSerialiser

    def get_queryset(self):
        return Record.objects.all()


class CreatePage(TemplateView):
    template_name = 'createpage.html'

class ViewPage(TemplateView):
    template_name = 'viewpage.html'