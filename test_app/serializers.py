from rest_framework.serializers import ModelSerializer
from .models import Record

class RecordSerialiser(ModelSerializer):
    class Meta:
        model = Record
        fields = [
            'text',
        ]